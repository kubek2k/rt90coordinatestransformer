package org.kubek2k.rt90coordinatesconverter;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GaussKrugerProjectionTest {

    private GaussKrugerProjection gaussKrugerProjection = new GaussKrugerProjection();

    @Test
    public void testAftonbladetCentral() throws Exception {
        RT90GeoCoordinates rt90GeoCoordinates = gaussKrugerProjection.getRT90(new WGS84Coordinates(59.332183, 18.053026));

        assertEquals(rt90GeoCoordinates.getX(), 6581200, 2.0);
        assertEquals(rt90GeoCoordinates.getY(), 1627935, 2.0);
    }

    @Test
    public void testSomeHotelRestaurant() throws Exception {
        RT90GeoCoordinates rt90GeoCoordinates = gaussKrugerProjection.getRT90(new WGS84Coordinates(59.3525207, 18.04338818));

        assertEquals(rt90GeoCoordinates.getX(), 6583446, 2.0);
        assertEquals(rt90GeoCoordinates.getY(), 1627311, 2.0);
    }

}
