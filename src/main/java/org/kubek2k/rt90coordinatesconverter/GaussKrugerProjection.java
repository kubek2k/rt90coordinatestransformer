package org.kubek2k.rt90coordinatesconverter;

/**
 * Copyright (C) 1991, 1999 Free Software Foundation, Inc.
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 *
 * [This is the first released version of the Lesser GPL.  It also counts
 * as the successor of the GNU Library Public License, version 2, hence
 * the version number 2.1.]
 *
 * Class copied from
 * @link https://code.google.com/p/oppna-program-hsatools-mobile-webservice
 *
 * and licensed under LGPL license
 *
 * Uses "Gauss konforma projektion" for conversion WGS84 <-> RT90.
 *
 * @link http://sv.wikipedia.org/wiki/Gauss_projektion
 * @link http://www.lantmateriet.se/templates/LMV_Page.aspx?id=4766
 * @link http://www.lantmateriet.se/upload/filer/kartor/geodesi_gps_och_detaljmatning/geodesi/Formelsamling/
 *      Gauss_Conformal_Projection.pdf
 * @link http
 *      ://www.lantmateriet.se/upload/filer/kartor/geodesi_gps_och_detaljmatning/Transformationer/SWEREF99_RT90_Samband
 *      /Transformationsparametrar_pdf.pdf
 * @link http://www.lantmateriet.se/templates/LMV_Page.aspx?id=3003
 * @link http://www.lantmateriet.se/upload/filer/kartor/geodesi_gps_och_detaljmatning/Kartprojektioner/Oversikt/
 *      kartprojektioner_oversikt.pdf
 * @link http://www.lantmateriet.se/upload/filer/kartor/geodesi_gps_och_detaljmatning/Kartprojektioner/Oversikt/Gauss-
 *      kruger_projektion.pdf
 * @link http://www.lantmateriet.se/templates/LMV_Page.aspx?id=5197
 * @link http://sv.wikipedia.org/wiki/RT_90
 *
 * @author Jonas Liljenfeldt, Know IT
 *
 */
public class GaussKrugerProjection {
    // GRS 80 Ellipsoid Characteristics:
    // Semi Major axis
    private static double majorAxis = 6378137.0;
    // Flattening
    private static double flattening = 1.0 / 298.2572221010;

    // RT90 0 gon V 0:-15 fields (Use around Stockholm)
    // Centrum meridian
    private static final String CM_0V = "18D03.2268\"E";
    // Scale factor
    private static final double K0_0V = 1.000005400000;
    // False North
    private static final double FN_0V = -668.844;
    // False East
    private static final double FE_0V = 1500083.521;

    public enum Gon {
        STOCKHOLM("18D03.2268\"E", 1.000005400000, -668.844, 1500083.521),
        OREBRO("15D48.22624306\"E", 1.00000561024, -667.711, 1500064.274),
        MALMO("13D33.376\"E", 1.000005800000, -667.130, 1500044.695),
        GOTEBORG("11D18.375\"E", 1.000006000000, -667.282, 1500025.141),
        UMEO("20D18.379\"E", 1.000005200000, -670.706, 1500102.765),
        LULEO("22D33.380\"E", 1.000004900000, -672.557, 1500121.846);

        String centrumMeridian;

        double scaleFactor;

        double falseNorth;

        double falseEast;

        private Gon(String centrumMeridian, double scaleFactor, double falseNorth, double falseEast) {
            this.centrumMeridian = centrumMeridian;
            this.scaleFactor = scaleFactor;
            this.falseNorth = falseNorth;
            this.falseEast = falseEast;
        }
    }

    // Variables
    private String cm;
    private double k0;
    private double fn;
    private double fe;
    private double a;
    private double b;
    private double c;
    private double d;
    private double beta1;
    private double beta2;
    private double beta3;
    private double beta4;
    private double e2;
    private double n;
    private double aHat;
    private double delta1;
    private double delta2;
    private double delta3;
    private double delta4;
    private double aStar;
    private double bStar;
    private double cStar;
    private double dStar;

    public GaussKrugerProjection(Gon gon) {
        if (gon != null) {
            this.cm = gon.centrumMeridian;
            this.k0 = gon.scaleFactor;
            this.fn = gon.falseNorth;
            this.fe = gon.falseEast;
        } else {
            throw new IllegalArgumentException("No GON provided");
        }
        this.initialize();
    }

    /**
     * Default constructor using 2.5 V 0:-15 (as in standard RT90).
     */
    public GaussKrugerProjection() {
        this(Gon.OREBRO);
    }

    /**
     * Calculate constants.
     */
    private void initialize() {
        this.e2 = flattening * (2.0 - flattening);
        this.n = flattening / (2.0 - flattening);
        this.aHat = majorAxis / (1.0 + this.n) * (1.0 + 0.25 * Math.pow(this.n, 2) + 1.0 / 64.0 * Math.pow(this.n, 4));
        this.a = this.e2;
        this.b = 1.0 / 6.0 * (5.0 * Math.pow(this.a, 2) - Math.pow(this.a, 3));
        this.c = 1.0 / 120.0 * (104.0 * Math.pow(this.a, 3) - 45.0 * Math.pow(this.a, 4));
        this.d = 1.0 / 1260.0 * 1237.0 * Math.pow(this.a, 4);

        this.beta1 = 0.5 * this.n - 2.0 / 3.0 * Math.pow(this.n, 2) + 5.0 / 16.0 * Math.pow(this.n, 3) + 41.0 / 180.0
                * Math.pow(this.n, 4);
        this.beta2 = 13.0 / 48.0 * Math.pow(this.n, 2) - 3.0 / 5.0 * Math.pow(this.n, 3) + 557.0 / 1440.0
                * Math.pow(this.n, 4);
        this.beta3 = 61.0 / 240.0 * Math.pow(this.n, 3) - 103.0 / 140.0 * Math.pow(this.n, 4);
        this.beta4 = 49561.0 / 161280.0 * Math.pow(this.n, 4);

        this.delta1 = 1.0 / 2.0 * this.n - 2.0 / 3.0 * Math.pow(this.n, 2) + 37.0 / 96.0 * Math.pow(this.a, 3) - 1.0
                / 360.0 * Math.pow(this.n, 4);
        this.delta2 = 1.0 / 48.0 * Math.pow(this.n, 2) + 1.0 / 15.0 * Math.pow(this.n, 3) - 437.0 / 1440.0
                * Math.pow(this.n, 4);
        this.delta3 = 17.0 / 480.0 * Math.pow(this.n, 3) - 37.0 / 840.0 * Math.pow(this.n, 4);
        this.delta4 = 4397.0 / 161280.0 * Math.pow(this.n, 4);

        this.aStar = this.e2 + Math.pow(this.e2, 2) + Math.pow(this.e2, 3) + Math.pow(this.e2, 4);
        this.bStar = -(1.0 / 6.0)
                * (7 * Math.pow(this.e2, 2) + 17.0 * Math.pow(this.e2, 3) + 30 * Math.pow(this.e2, 4));
        this.cStar = 1.0 / 120.0 * (224 * Math.pow(this.e2, 3) + 889.0 * Math.pow(this.e2, 4));
        this.dStar = -(1.0 / 1260.0) * 4279.0 * Math.pow(this.e2, 4);
    }

    /**
     * Calculate grid coordinates with Gauss-Kruger projection method.
     *
     * @param latitude Latitude in decimal format.
     * @param longitude Longitude in decimal format.
     */
    private double [] calcGaussKrugerProjectionFromGeodeticToGrid(final double latitude, final double longitude) {
        // Compute the Conformal Latitude
        double phiStar = latitude
                - Math.sin(latitude)
                * Math.cos(latitude)
                * (this.a + this.b * Math.pow(Math.sin(latitude), 2) + this.c * Math.pow(Math.sin(latitude), 4) + this.d
                * Math.pow(Math.sin(latitude), 6));

        // Difference in longitude
        double dLon = longitude - getLatLongRadiansDecimal(this.cm, true);

        // Get Angles:
        double chi = Math.atan(Math.tan(phiStar) / Math.cos(dLon));

        // Since Atanh isn't represented in the Math-class
        // we'll use a simplification that holds for real z < 1
        // Ref:
        // http://mathworld.wolfram.com/InverseHyperbolicTangent.html
        double z = Math.cos(phiStar) * Math.sin(dLon);
        double eta = 0.5 * Math.log((1.0 + z) / (1.0 - z));

        // Calculate the carthesian (grid) coordinates in RT90
        double rt90X = this.k0
                * this.aHat
                * (chi + this.beta1 * Math.sin(2.0 * chi) * Math.cosh(2.0 * eta) + this.beta2 * Math.sin(4.0 * chi)
                * Math.cosh(4.0 * eta) + this.beta3 * Math.sin(6.0 * chi) * Math.cosh(6.0 * eta) + this.beta4
                * Math.sin(8.0 * chi) * Math.cosh(8.0 * eta)) + this.fn;

        double rt90Y = this.k0
                * this.aHat
                * (eta + this.beta1 * Math.cos(2.0 * chi) * Math.sinh(2.0 * eta) + this.beta2 * Math.cos(4.0 * chi)
                * Math.sinh(4.0 * eta) + this.beta3 * Math.cos(6.0 * chi) * Math.sinh(6.0 * eta) + this.beta4
                * Math.cos(8.0 * chi) * Math.sinh(8.0 * eta)) + this.fe;

        return new double[] {rt90X, rt90Y};
     }

    /**
     * Calculate grid coordinates with Gauss-Kruger projection method.
     *
     * @param x Latitude in RT 90.
     * @param y Longitude in RT 90
     */
    private double [] calcGaussKrugerProjectionFromGridToGeodetic(final int x, final int y) {
        double chi = (x - this.fn) / (this.k0 * this.aHat);
        double eta = (y - this.fe) / (this.k0 * this.aHat);

        double chiPrim = chi - this.delta1 * Math.sin(2 * chi) * Math.cosh(2 * eta) - this.delta2 * Math.sin(4 * chi)
                * Math.cosh(4 * eta) - this.delta3 * Math.sin(6 * chi) * Math.cosh(6 * eta) - this.delta4
                * Math.sin(8 * chi) * Math.cosh(8 * eta);

        double etaPrim = eta - this.delta1 * Math.cos(2 * chi) * Math.sinh(2 * eta) - this.delta2 * Math.cos(4 * chi)
                * Math.sinh(4 * eta) - this.delta3 * Math.cos(6 * chi) * Math.sinh(6 * eta) - this.delta4
                * Math.cos(8 * chi) * Math.sinh(8 * eta);

        // Compute the Conformal Latitude
        double phiStar = Math.asin(Math.sin(chiPrim) / Math.cosh(etaPrim));

        // Difference in longitude
        double dLon = Math.atan(Math.sinh(etaPrim) / Math.cos(chiPrim));

        // Eventually the latitude and longitude angles are calculated.
        double lon = getLatLongRadiansDecimal(this.cm, true) + dLon;

        double lat = phiStar
                + Math.sin(phiStar)
                * Math.cos(phiStar)
                * (this.aStar + this.bStar * Math.pow(Math.sin(phiStar), 2) + this.cStar
                * Math.pow(Math.sin(phiStar), 4) + this.dStar * Math.pow(Math.sin(phiStar), 6));

        return new double[] {lat, lon};
     }

    /**
     * {@inheritDoc}
     */
    public RT90GeoCoordinates getRT90(WGS84Coordinates wgs84Coordinates) {
        // Degrees -> radians
        double lat = wgs84Coordinates.getGeoLatitude() * Math.PI / 180.0;
        double lon = wgs84Coordinates.getGeoLongitude() * Math.PI / 180.0;

        // Calculate Projection on the RT90-grid
        double [] gridXY = this.calcGaussKrugerProjectionFromGeodeticToGrid(lat, lon);

        // Return x,y array. Not nice conversion but works :)
        return new RT90GeoCoordinates(Integer.parseInt(String.valueOf(Math.round(gridXY[0]))),
                Integer.parseInt(String.valueOf(Math.round(gridXY[1]))));
    }

    /**
     * {@inheritDoc}
     */
    public WGS84Coordinates getWGS84(RT90GeoCoordinates rt90Coordinates) {
        int rt90X = rt90Coordinates.getX();
        int rt90Y = rt90Coordinates.getY();

        // Calculate geodetic coordinates from RT90 coordinates
        double [] coordinatesInRadians = this.calcGaussKrugerProjectionFromGridToGeodetic(rt90X, rt90Y);

        // Convert WGS84 coordinates from radians to decimal degrees
        double wgs84LatDegree = coordinatesInRadians[0] / (Math.PI / 180.0);
        double wgs84LonDegree = coordinatesInRadians[1] / (Math.PI / 180.0);

        return new WGS84Coordinates(wgs84LatDegree, wgs84LonDegree);
    }

    /**
     * Parse NMEA-String.
     *
     * @param latOrLong Latitude or longitude in NMEA format
     * @param isLong True if longitude, false if latitude.
     * @return Latitude or longitude in radians as decimal value.
     */
    static double getLatLongRadiansDecimal(final String latOrLong, final boolean isLong) {
        // Get Hours (up to the 'D')
        double deciLatLon = Double.parseDouble(latOrLong.substring(0, latOrLong.indexOf("D")));

        // Remove it once we've used it
        String remainder = latOrLong.substring(latOrLong.indexOf("D") + 1);

        // Get Minutes (up to the '.') and3872648 divide by Minutes/Hour
        deciLatLon += Double.parseDouble(remainder.substring(0, remainder.indexOf("."))) / 60.0;

        // Remove it once we've used it
        remainder = remainder.substring(remainder.indexOf(".") + 1);

        // Get Seconds (up to the '"') and divide by Seconds/Hour
        String sec = remainder.substring(0, remainder.indexOf("\""));
        // Insert a dot to prevent the time from flying away...
        deciLatLon += Double.parseDouble(new StringBuilder(sec).insert(2, ".").toString()) / 3600.0;

        // Get the Hemisphere String
        remainder = remainder.substring(remainder.indexOf("\"") + 1);
        if (isLong && "S".equals(remainder) || !isLong && "W".equals(remainder)) {
            // Set us right
            deciLatLon = -deciLatLon;
        }
        // And return (as radians)
        return deciLatLon * Math.PI / 180.0;
    }
}
