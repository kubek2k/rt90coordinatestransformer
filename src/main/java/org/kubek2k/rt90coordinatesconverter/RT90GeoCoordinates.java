package org.kubek2k.rt90coordinatesconverter;

public class RT90GeoCoordinates {
    private int x;

    private int y;

    public RT90GeoCoordinates(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return "RT90GeoCoordinates{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
