package org.kubek2k.rt90coordinatesconverter;

public class WGS84Coordinates {

    private double geoLatitude;

    private double geoLongitude;

    public WGS84Coordinates(double geoLatitude, double geoLongitude) {
        this.geoLatitude = geoLatitude;
        this.geoLongitude = geoLongitude;
    }

    public double getGeoLatitude() {
        return geoLatitude;
    }

    public double getGeoLongitude() {
        return geoLongitude;
    }

    @Override
    public String toString() {
        return "WGS84Coordinates{" +
                "geoLatitude=" + geoLatitude +
                ", geoLongitude=" + geoLongitude +
                '}';
    }
}
